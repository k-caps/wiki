
***The standard tool to benchmark PostgreSQL in pgbench.***   
It runs the same sequence of SQL commands over and over, possibly in multiple concurrent database sessions, and then
       calculates the average transaction rate (transactions per second).   
By default, pgbench tests a scenario that is loosely based on TPC-B, involving five SELECT, UPDATE, and INSERT commands
       per transaction. However, it is easy to test other cases by writing your own transaction script files.  

 Typical output from pgbench looks like:

           transaction type: TPC-B (sort of)
           scaling factor: 10
           query mode: simple
           number of clients: 10
           number of threads: 1
           number of transactions per client: 1000
           number of transactions actually processed: 10000/10000
           tps = 85.184871 (including connections establishing)
           tps = 85.296346 (excluding connections establishing)

The first six lines report some of the most important parameter settings. The next line reports the number of transactions completed and intended (the latter being just the product of
       number of clients and number of transactions per client); these will be equal unless the run failed before completion. (In -T mode, only the actual number of transactions is printed.) The
       last two lines report the number of transactions per second, figured with and without counting the time to start database sessions.
****
***initialize pgbench***
****
The default TPC-B-like transaction test requires specific tables to be set up beforehand.    
pgbench should be invoked with the -i (initialize) option to create and populate these tables.  
       (When you are testing a custom script, you don't need this step, but will instead need to do whatever setup your test needs.)   
Initialization looks like:  
`pgbench -i [ other-options ] dbname`

pgbench -i creates four tables pgbench_accounts, pgbench_branches, pgbench_history, and pgbench_tellers, destroying any existing tables of these names. Be very careful to use another
           database if you have tables having these names!


       The tables initially contain this many rows:

           table                   # of rows
           ---------------------------------
           pgbench_branches        1
           pgbench_tellers         10
           pgbench_accounts        100000
           pgbench_history         0
You can (and, for most purposes, probably should) increase the number of rows by using the -s (scale factor) option. The -F (fillfactor) option might also be used at this point.

Once you have done the necessary setup, you can run your benchmark with a command that doesn't include -i, that is

`pgbench [ options ] dbname`

In nearly all cases, you'll need some options to make a useful test. The most important options are -c (number of clients), -t (number of transactions), -T (time limit), and -f (specify a
       custom script file), With the -l option, pgbench writes the time taken by each transaction to a log file.

****
   **What is the “Transaction” Actually Performed in pgbench?**
****
       The default transaction script issues seven commands per transaction:

        1. BEGIN;

        2. UPDATE pgbench_accounts SET abalance = abalance + :delta WHERE aid = :aid;

        3. SELECT abalance FROM pgbench_accounts WHERE aid = :aid;

        4. UPDATE pgbench_tellers SET tbalance = tbalance + :delta WHERE tid = :tid;

        5. UPDATE pgbench_branches SET bbalance = bbalance + :delta WHERE bid = :bid;

        6. INSERT INTO pgbench_history (tid, bid, aid, delta, mtime) VALUES (:tid, :bid, :aid, :delta, CURRENT_TIMESTAMP);

        7. END;

       If you specify -N, steps 4 and 5 aren't included in the transaction. If you specify -S, only the SELECT is issued.


