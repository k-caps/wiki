---
title: "PGAdmin backup fails with exit code 1"
---

Checking the postgresql logs in the server, we can see that there is a "permission denied" for some schema. The problem is that the user that tries to do the backup does not have sufficient permissions on schemas that are created by extensions and the tables, sequences in them. The solution for this can be checking what schemas that are created by extensions are installed on the server, and granting sufficient permissions:

```sql
GRANT USAGE ON SCHEMA [schema_name] TO [db_user];
GRANT SELECT ON ALL SEQUENCES IN SCHEMA [schema_name] TO [db_user];
GRANT SELECT ON ALL TABLES IN SCHEMA [schema_name] to [db_user];
```
