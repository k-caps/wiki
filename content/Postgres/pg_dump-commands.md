For small amounts of data:
---
*On the source DB server*  
`sudo -U postgres pg_dump <source_dbname> -T <tablename1> -T <tablename2 > dumpfile_name.sql`

*On the target DB server*  
`sudo -U postgres psql -d <target_dbname> -f dumpfile_name.sql`

For large amounts of data:
---

**Basic dump & restore**  
*On the source DB server*  
`time pg_dump -d <source_dbname> -Fd -f /var/lib/pgsql/<dump_directory_name> -Z <compression level> -j <number of threads> -n <schemaname>`

*On the target DB server*  
`time pg_restore -d <target_dbname> /var/lib/pgsql/<dump_directory_name> -j <number of threads> -n <schemaname> -c --if-exists`


<h3>Real life example:</h3>  
These commands dump a database with a compression level of 0, using 4 threads.  
Then, the resulting directory dump is imported while running in the background, saving both stout and stderr into a single log file, and the whole thing is timed, so we know how long it took.

`time pg_dump -d datastore -Fd -f /var/lib/pgsql/datastore_dump_22-11 -Z 0 -j 4`   
`nohup bash -c 'time pg_restore -d datastore-dev /var/lib/pgsql/datastore_dump_22-11 -j 4 -c --if-exists > import.log 2>&1' >> import.log 2>&1 &` 

<h3>Example of streaming dump_restore, with no dump file or directory</h3>  
 
`pg_dump -Fc -v --<source_server> --port=5432 --username=<username> --dbname=<dbname> | pg_restore -v --no-owner --host=<target-server> --port=5432 --username=<username> --dbname=<dbname>`
