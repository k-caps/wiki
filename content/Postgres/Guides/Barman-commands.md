---
title: "Barman command reference sheet"
date: "2023-07-09T17:42:48+03:00"
---


First off, we can check the status of a cluster in Barman with `barman check <servername>`.  
```
barman check server.com
Server server.com:
        PostgreSQL: OK
        is_superuser: OK
        PostgreSQL streaming: OK
        wal_level: OK
        replication slot: OK
        directories: OK
        retention policy settings: OK
        backup maximum age: OK (interval provided: 7 days, latest backup age: 3 hours, 17 minutes, 31 seconds)
        compression settings: OK
        failed backups: OK (there are 0 failed backups)
        minimum redundancy requirements: OK (have 9 backups, expected at least 0)
        pg_basebackup: OK
        pg_basebackup compatible: OK
        pg_basebackup supports tablespaces mapping: OK
        systemid coherence: OK
        pg_receivexlog: OK
        pg_receivexlog compatible: OK
        receive-wal running: OK
        archiver errors: OK
```
**To list backups for a given server:**  
`barman list-backup <servername>`  
**To delete a given backup:**  
Copy the backup ID (2nd column) and delete it:  
`barman delete <servername> <backup ID>`  

**To run a backup**  
`barman backup --wait --reuse-backup link <servername> >> /var/log/barman/<servername>.log`  

### Real life example:  
```
barman list-backup server.com
server.com 20210530T120013 - Sun May 30 12:05:09 2021 - Size: 6.1 GiB - WAL Size: 0 B (tablespaces: temp_tbs:/temp_tbs)
server.com 20210530T000012 - Sun May 30 00:05:56 2021 - Size: 6.1 GiB - WAL Size: 14.2 MiB (tablespaces: temp_tbs:/temp_tbs)
barman delete server.com 20210530T120013`
```

**To delete and recreate a server record in Barman:**  
```
barman receive-wal --stop <servername>
barman receive-wal --drop-slot <servername>
barman receive-wal --create-slot <servername>
barman receive-wal --reset <servername>
barman cron
barman switch-wal --force --archive <servername>
```

**To delete all of a server's data without deleting the server itself:**  
`rm -rf /var/lib/barman/<servername>/*`  

