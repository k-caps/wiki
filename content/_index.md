---
archetype: "home"
---

**Welcome to k-caps's wiki & snippets website!**
=================================
This website is meant as a living document of my experience - whenever I think I might need to look something up, it gets added to this wiki.  

To view my code, you can go to https://gitlab.com/k-caps/, and especially https://gitlab.com/k-caps/scripts.  


