Sometimes you want to run Ansible and store it's output to a log file to examine later. However, simply redirecting `ansible-playbook > log` does not work properly, and will cause ansible's output to look garbled, both on stdout and in the resulting log file.  
This is because many linux commands do not use color unless their target output is an interactive terminal.  
To get around this, we can use the `script` command as so:  
`script -q -c "ansible-playbook -i inventory_file playbook_file" logfile`  
