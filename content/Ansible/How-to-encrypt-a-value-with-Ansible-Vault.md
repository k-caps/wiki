---
title: "How to encrypt values with Ansible Vault"
date: 2023-07-10
---


To create a new encrypted variable with vault, use the following command:  
`ansible-vault encrypt_string --vault-id @prompt 'testvalue' --name testvalue`.  
The output will be something like this:  
```yaml
testvalue: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          89763234613032646432656163454321366339323561316363633263363866646234343562343936
          3666316361666233303830353636373335666237656261360a613966353630663766393332396433
          37643231306133616662616264613461656637653432366534316535663234306239653633306463
          3636346663393638300a613431663839393232366533663861353061626239356432366133333831
          7251
```

This can be used as-is in Ansible code, either in the inventory files, or in a task as `set_fact`, or however else you like.  
Then, when you run ansible with `--ask-vault-pass`, and you use the same password you used when encrypting, the value will be decrypted and available to you as a regular variable called "testvalue" (in this case).
