---
title: "Tmux primer"
date: 2023-07-10
---



Tmux, short for "Terminal Multiplexer", is a very useful tool that has some  features that are extra helpful for DBaaS.  

The features that are most useful for us are:
* Any command executed in a tmux session is automatically running in the background - you never have to worry about nohup for long commands, even if your connection dies or you close your moba tab.
* Exiting and re-entering sessions is very easy - which allows you to have several distinct shell environments at the same time.
* You can split a session into multiple "panes", with each pane emulating a separate terminal window. This is similar to MobaXterm's MultiExec mode, but with far greater flexibilty and control.

**IMPORTANT:**  
Mouse does not work properly in tmux. You can get around it with some commands but it is not as simple as a normal terminal. This is because we have an old version of tmux in our network.  
The most critical one is that you have to use <kbd>shift</kbd> while scrolling to scroll. 
### tmux sessions:

Simply type `tmux` on any of our SSH gateways to begina tmux session.  
This will open a tmux shell environment - which is identical to a normal shell environment expet for a statusbar at the bottom of the screen.  

Let's demonstrate background jobs:  
`sleep 1000`  
Now close your moba tab, with the <kbd>x</kbd> button.  In a normal shell session, this would kill the sleep process.   
Hoever, since we ran our command under tmux,  it is still running.  
To see this, reconnect to the gateway in a new tab, then run:   
`ps -ef | grep sleep`.  
As you can see, the process is still running.  
To re-enter our environment, we have to find our tmux session. To do that, run:  
`tmux list-session`. (`tmux ls` for pros)    
You will see a list of all open tmux sessions.  Because they are simply numbered, in the event of multiple sessions, you should invoke new tmux sessions with names, like so:   
`tmux new -s <session name>`. This will make the output of `tmux ls` easier to use.   
In any case, once you know what session you want (easy when you only have one!), run this:  
`tmux attach-session -t <session identifier>` where `<session identifier>` is either the id number or session name you got from `tmux ls`.  

### tmux commands:   
Tmux uses the concept of a command key, similar to emacs.  The default command key is <kbd>ctrl</kbd>+<kbd>b</kbd>.  
However, in our environment this has been changed to <kbd>ctrl</kbd>+<kbd>z</kbd>, to make it more comfortable to invoke.  
In tmux standard notation, we write `C-` to mean <kbd>ctrl</kbd>, and then whatever follows the `-` is the second key. So `C-z` = <kbd>ctrl</kbd>+<kbd>z</kbd>.  
Once you invoke the command key, you can use tmux commands. These are similar in nature to vim or emacs commands, like vim's `:set number`.  
A simple example is detaching from a tmux session without actually killing the whole tab or window:  
`C-z d` (short for detach).  
This  notation means "<kbd>ctrl</kbd>+<kbd>z</kbd> **followed by a separate keypress,** <kbd>d</kbd>"   
Let's reattach our session again:   
`tmux attach-session -t 0`  

Some other commands which are very useful:  
`C-z C-z` to actually do a <kbd>ctrl</kbd><kbd>z</kbd>, in case you want to pause a command like you would normally, without leaving your session.  

`C-z |` splits your current active window with a vertical pane.  
`C-z -` does the same horizontally.  

When you have multiple panes open, you can use the arrow keys to move bweteen them, like so:  
<kbd>alt</kbd>+<kbd>&larr;</kbd>    
<kbd>alt</kbd>+<kbd>&rarr;</kbd>  
<kbd>alt</kbd>+<kbd>&uarr;</kbd>    
<kbd>alt</kbd>+<kbd>&darr;</kbd>

`C-x` sends input to all panes at once, just like multiexec mode does by default. The same command sets you back to single pane input.  

`C-z z` Maximizes your currently active pane, without closing the others. (No more having to disable all unwanted multiexec windows or opening and closing multiexec mode)  
You can unmaximize it with the same command again.  


