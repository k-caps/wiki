---
title: "How to view bash History with a timestamp"
date: 2023-07-10
---


Just run:  
`HISTTIMEFORMAT="%d/%m/%y %T "`  
For the remainder of your session, `history` will output including a nice timestamp.
