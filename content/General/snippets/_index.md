---
title: "Useful Code Snippets"
date: "2023-07-09T17:42:48+03:00"
archetype: "chapter"
---

Here is where you can view short bits of code which can be used independently of anything else, just useful stuff that's nice to not have to google.  


